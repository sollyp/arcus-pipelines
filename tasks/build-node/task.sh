#!/bin/bash

set -eux

cd resource-weather-ui \
    && npm install \
    && npm run build