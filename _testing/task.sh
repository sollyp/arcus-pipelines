#!/bin/bash

set -eux

cd resource-weather-ui \
   && docker build -t weather-ui . \
   && docker push arcusassign/weather-ui 
   